package MySocialNetworking;

import java.util.ArrayList;

/**
	 * To create a user Class for Whatsapp Application
	 * @author thriveni
	 *
	 */

	public class User {
		
		ArrayList<String>phone=new ArrayList<String>();
		public  String name;     //To represents the name
		public  String phoneNo;  //To represents the phoneNo
		public  String password;//To represents the password
		public  String status;  //To represent the status
	/**
	 * To create a constructor with arguments for the User class
	 * @param name
	 * @param phoneNo
	 * @param password
	 * @param status
	 */
		public User(String name, String phoneNo,String password,String status)
		{
			this.name=name;         //using this statement
			this.phoneNo=phoneNo;   //using this statement
			this.password=password; //using this statement
			this.status=status;
			System.out.println(this);
			phone.add(phoneNo);
	
		}			
		/**
		 * override the default toString method of Message 
		 * so that message can be printed in human readable format
		 */
		@Override
		public String toString() 
		   {
			String StringToReturn = "";
			StringToReturn += "name      :     "+this.name+                     "\n";
			StringToReturn += "phoneNo   :     "+this.phoneNo+                  "\n";
			StringToReturn += "password  :     "+this.password+                 "\n";
			StringToReturn += "status    :     "+this.status+                   "\n";			
			return StringToReturn;
			}
}
