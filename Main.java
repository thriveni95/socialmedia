package MySocialNetworking;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class Main 
{
	public static void main(String[] args) {
		/*
		 * To Create UserCollection Object to call all the methods in the UserCollectionList
		 */
			UserCollection uc=new UserCollection();
			/*
			 * By using UserCollection object calling the CreateNewUser method having parameters
			 * 
			 * @param name
			 * @param phnno
			 * @param password
			 * @param status
			 */
			System.out.println("   **  Print the list of  all users **   ");
			System.out.println("...............................");
			uc.createNewUser("Thriveni", "7281364540", "@bhjagd", "read");
			System.out.println("...............................");
			uc.createNewUser("sekhar", "9999999990", "@bhjagd", "read");
			System.out.println("...............................");
			uc.createNewUser("chandra", "7281364542", "@bhjagd", "read");
			System.out.println("...............................");
			uc.createNewUser("sanvika", "7281364541", "@bhjagd", "read");
			System.out.println("...............................");
			uc.createNewUser("veera", "7281364543", "@bhjagd", "read");
			System.out.println("...............................");
			uc.createNewUser("pushpa", "7281364544", "@bhjagd", "read");
			System.out.println("...............................");
			uc.createNewUser("siva", "7281364545", "@bhjagd", "read");
			
			/*
			 * By using UserCollection object calling the SendMessages method having parameters
			 * 
			 * @param Senderphnno
			 * @param Receiverphnno
			 * @param messagebody
			 * 
			 */
			System.out.println("   **  Sent messages List **   ");
			System.out.println("            ");
			uc.SendMessages("9999999990","8878647587","hi...........How are u?");
			System.out.println("Date & Time         :"+"       "+java.time.LocalDateTime.now());
			System.out.println("...............................");
			uc.SendMessages("8919230149","9515318930","good morning");
            System.out.println("Date & Time         :"+"       "+java.time.LocalDateTime.now());
			System.out.println("...............................");
			uc.SendMessages("9515318930","8886021879","when will u come");
            System.out.println("Date & Time         :" +"      "+java.time.LocalDateTime.now());
			System.out.println("...............................");
			uc.SendMessages("9553513658","8878647587","i am in hyderabad");
            System.out.println("Date & Time         :"+"        " +java.time.LocalDateTime.now());
			System.out.println("...............................");
			/*
			 * By using UserCollection object calling the ReceiverMessages method having parameters
			 * 
			 * @param Senderphnno
			 * @param Receiverphnno
			 * @param messagebody
			 * 
			 */
			System.out.println("   **  Received messages List **   ");
			System.out.println("            ");
			uc.SendMessages("8878647587","9999999990","hi...........I am fine.What about u?");
			System.out.println("Date & Time         :"+"       "+java.time.LocalDateTime.now());
			System.out.println("...............................");
			uc.SendMessages("9515318930","8919230149","good morning");
            System.out.println("Date & Time          :"+"       "+java.time.LocalDateTime.now());
			System.out.println("...............................");
			uc.SendMessages("8886021879","9515318930","Tomorrow onwords");
            System.out.println("Date & Time         :" +"      "+java.time.LocalDateTime.now());
			System.out.println("...............................");
			uc.SendMessages("8878647587","9553513658","ok");
            System.out.println("Date & Time         :"+"        " +java.time.LocalDateTime.now());
			System.out.println("...............................");
			
			
			
			
			
			
			
			
		
	
}
}

