package MySocialNetworking;

	import java.util.ArrayList;
import java.time.LocalDateTime;
import java.util.Collection;
import java.util.Date;
	/**
	 * To create a Message class to implement the messages
	 * by using ArrayList
	 * @author Thriveni
	 *
	 */
	public class Message {
		ArrayList<String> messageList = new ArrayList<String>();
		private String sendmessage;
		private String recievemessage;
		private String msgbody;
		
	/**
	 * To create a constructor with parameters 
	 * @param senderphnno
	 * @param receiverphnno
	 * @param msgbody
	 */
	public Message(String senderphnno, String receiverphnno, String msgbody){
		this.sendmessage=senderphnno;  //using this statement
		this.recievemessage=receiverphnno;   //using this statement
		this.msgbody=msgbody;//using this statement
		System.out.println(this);
		}
	 
	/**
	 * override the default toString method of Message 
	 * so that message can be printed in human readable format
	 */
	@Override
	public String toString() {
		String returnToMe=" ";
		returnToMe+="sender phnno  :        "+this.sendmessage+  "\n";
		returnToMe+="receiver phnno:        "+this.recievemessage+   "\n";
		returnToMe+="message body  :        "+this.msgbody+      "\n";
		return returnToMe;
	}
}
