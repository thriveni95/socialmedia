package MySocialNetworking;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
/**
 * To create a UserCollection class to implement the UserCollection
 * by using ArrayList
 * @author Thriveni
 *
 */

public class UserCollection
{
	ArrayList<String> userList  = new ArrayList<String>();
	ArrayList<String> sendList  = new ArrayList<String>();
	ArrayList<String> receiveList  = new ArrayList<String>();
	/**
	 * To create createNewUser method using parameters
	 * by using ArrayList and addUser
	 
	 */
	@SuppressWarnings("unchecked")
	public boolean createNewUser(String name, String phoneNo, String password,String status)
	{
		try
		{
			User addUser=new User(name, phoneNo, password,status);
			userList.addAll((Collection<? extends String>) addUser);
			return true;
		}
		catch(Exception e)
		{
		
		return false;
		
	}
	}
	/**
	 * To create Sendmessages method using parameters
	 * by using ArrayList and addSendList
	 
	 */
		@SuppressWarnings("unchecked")
		public boolean SendMessages(String senderphnno, String receiverphnno, String msgbody)
		{
			try
			{
				Message addSendList=new Message(senderphnno,receiverphnno,msgbody);
				userList.addAll((Collection<? extends String>) addSendList);
				return true;
			}catch(Exception e) {
			
			return false;
			
		}
		}
		/**
		 * To create Receivemessages method using parameters
		 * by using ArrayList and addReceiveList
		 
		 */
		
			@SuppressWarnings("unchecked")
			public boolean ReceiveMessages(String senderphnno, String receiverphnno, String msgbody)
			{
				try
				{
					Message addReceiveList=new Message(senderphnno,receiverphnno,msgbody);
					userList.addAll((Collection<? extends String>) addReceiveList);
					return true;
				}catch(Exception e) {
				
				return false;
				
			}
		
		
		
		
		
}
	
}
